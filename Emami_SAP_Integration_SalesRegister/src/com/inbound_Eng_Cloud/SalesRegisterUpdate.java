//Version:1.00
/**
 ****@Program: Insert into the SalesRegister object in engagement cloud based on the values from the sap id in excel 
 * */
package com.inbound_Eng_Cloud;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.text.SimpleDateFormat;
import java.util.Calendar;

import org.json.JSONArray;
import org.json.JSONObject;

public class SalesRegisterUpdate {
	public static LogFile logfil = new LogFile();
	int resCode = 0;
	String jsonformat = "";
	//String basequery="https://emwy-test.fa.em3.oraclecloud.com:443/crmRestApi/resources/11.13.18.05/";
	String basequery="https://emwy.fa.em3.oraclecloud.com:443/crmRestApi/resources/11.13.18.05/";
	String username ="API USER";
	//String username ="Admin Admin";
	//String password ="EMAMI_123";
	String password ="API@1234";
	private static final String strdelimetercomma = ",";
	public static void main(String[] args) {
		SalesRegisterUpdate salesreg=new SalesRegisterUpdate();
		System.out.println("Started");
		//File folder = new File("E:\\Emami\\TEST\\"); /*path to your folder*/
		 File folder = new File("D:\\FTP\\staging\\CRM\\Salesregister\\Outbound\\");
		String[] filesPresent = folder.list();
	    System.out.println(filesPresent.length);
	    if(filesPresent.length==0){
	        System.out.println("Nothing to delete");
	        
	    }else{
	        for(String fileName : filesPresent){  
	        	System.out.println(fileName);
	        	System.out.println("Month May"+getYesterdayDate());
	            if(fileName.toLowerCase().endsWith(".csv") && fileName.toLowerCase().contains("sregister") && fileName.contains(getYesterdayDate().toString()) ){
	              System.out.println("if passes");
	                File file = new File(fileName);
	                logfil.passfileName(file.getName());
	              
		String downloadfilename=file.getName();
		moveFilesInitial(folder.getAbsolutePath(),downloadfilename) ;
	            }
	        }
	    }
		//salesreg.getExcelDetails();
	}
	public void getExcelDetails()
	{
	BufferedReader br = null;
	try {
		System.out.println("Started");
		//File folder = new File("E:\\Emami\\TEST\\"); /*path to your folder*/
		 File folder = new File("D:\\FTP\\staging\\CRM\\Salesregister\\Archive\\");
		String[] filesPresent = folder.list();
	    System.out.println(filesPresent.length);
	    if(filesPresent.length==0){
	        System.out.println("Nothing to delete");
	        
	    }else{
	        for(String fileName : filesPresent){  
	        	System.out.println(fileName);
	        	System.out.println("yesterday"+getYesterdayDate());
	            if(fileName.toLowerCase().endsWith(".csv") && fileName.toLowerCase().contains("sregister") && fileName.contains(getYesterdayDate().toString()) ){
	              System.out.println("if passes");
	                File file = new File(fileName);
	                logfil.passfileName(file.getName());
	              
		String downloadfilename=file.getName();
	//br = new BufferedReader(new FileReader("E:\\Emami\\TEST\\"+ downloadfilename ));// FileContents
		br = new BufferedReader(new FileReader("D:\\FTP\\staging\\CRM\\Salesregister\\Archive\\"+downloadfilename ));
		String line = "";
		//br.readLine();
		logfil.getStringLog("sales org" + ","+"Distribution Channel"+","+"Division"+","+"InvoiceNo"+","+"InvoiceDate"+","+"billingType"+","+"srcLocation"+","+"custDepot"+","+"billToParty"+","+"shipToParty"+","+"destn"+","+"Material"+","+"grossVal"+","+"SONumber"+","+"HSNCode"+","+"Response"+"SalesRegisterNo");
			while ((line = br.readLine()) != null) {
				String sales_org=null,distChnl=null,division=null,invoiceNo=null,invoiceDate=null,billingType=null,srcLoc=null,custDepot=null,billToParty=null,shipToParty=null,destn=null,material=null,salesQty=null,grossVal=null,SONum=null,HSNCode=null;
				String[] salesRegisterDetails = line.split(strdelimetercomma);
				//System.out.println("outstandingDetails.length::"+outstandingDetails.length);
				if (salesRegisterDetails.length > 0) {
					if (salesRegisterDetails[0] != null) {
						sales_org = salesRegisterDetails[0];
					} else {
						sales_org = null;
					}
					if (salesRegisterDetails[1] != null) {
						distChnl = salesRegisterDetails[1];
					} else {
						distChnl = null;
					}
					if (salesRegisterDetails[2] != null) {
						division = salesRegisterDetails[2];
					} else {
						division = null;
					}
					if (salesRegisterDetails[3] != null) {
						invoiceNo = salesRegisterDetails[3];
					} else {
						invoiceNo = null;
					}
					if (salesRegisterDetails[4] != null) {
						invoiceDate = salesRegisterDetails[4];
					} else {
						invoiceDate = null;
					}
					if (salesRegisterDetails[5] != null) {
						billingType = salesRegisterDetails[5];
					} else {
						billingType = null;
					}
					if (salesRegisterDetails[6] != null) {
						srcLoc = salesRegisterDetails[6];
					} else {
						srcLoc = null;
					}
					if (salesRegisterDetails[7] != null) {
						custDepot = salesRegisterDetails[7];
					} else {
						custDepot = null;
					}
					if (salesRegisterDetails[8] != null) {
						billToParty = salesRegisterDetails[8];
					} else {
						billToParty = null;
					}
					if (salesRegisterDetails[9] != null) {
						shipToParty = salesRegisterDetails[9];
					} else {
						shipToParty = null;
					}
					if (salesRegisterDetails[10] != null) {
						destn = salesRegisterDetails[10];
					} else {
						destn = null;
					}
					if (salesRegisterDetails[11] != null) {
						material = salesRegisterDetails[11];
					} else {
						material = null;
					}
					if (salesRegisterDetails[12] != null) {
						Float num = Float.parseFloat(salesRegisterDetails[12]);
					    String numberAsString = String.format ("%.2f", num);
						salesQty = numberAsString;
					} else {
						salesQty = null;
					}
					if (salesRegisterDetails[16] != null) {
						Float num = Float.parseFloat(salesRegisterDetails[16]);
					    String numberAsString = String.format ("%.2f", num);
						grossVal = numberAsString;
					} else {
						grossVal = null;
					}
					if (salesRegisterDetails[17] != null) {
						SONum = salesRegisterDetails[17];
					} else {
						SONum = null;
					}
					if (salesRegisterDetails[18] != null) {
						HSNCode = salesRegisterDetails[18];
					} else {
						HSNCode = null;
					}
					
					System.out.println("sales org::" + sales_org+"::Distribution Channel::"+distChnl+"::division::"+division+"::invoiceNo::"+invoiceNo+"::invoiceDate::"+invoiceDate+"::billingType::"+billingType+"::srcLocation::"+srcLoc+"::custDepot::"+custDepot+"::billToParty::"+billToParty+"::shipToParty::"+shipToParty+"::destn"+destn+"::material::"+material+"::grossVal::"+grossVal+"::SONum::"+SONum+"::HSNCode::"+HSNCode);
					String logMsg= sales_org+","+distChnl+","+division+","+invoiceNo+","+invoiceDate+","+billingType+","+srcLoc+","+custDepot+","+billToParty+","+shipToParty+","+destn+","+material+","+grossVal+","+SONum+","+HSNCode;
					JSONObject outstandObj=new JSONObject();
					
					outstandObj.put("SalesOrganisation_c", sales_org);
					outstandObj.put("DistrictChannel_c", distChnl);
					outstandObj.put("Division_c", division);
					outstandObj.put("InvoiceNumber_c", invoiceNo);
					outstandObj.put("InvoiceDate_c", invoiceDate);
					outstandObj.put("BillingType_c", billingType);
					
					long src_loc_Id = getSourceLocation(srcLoc);
					if(src_loc_Id != 0)
					outstandObj.put("SourceLocation_Id_c", src_loc_Id);
					long cust_depot_Id = getSourceLocation(custDepot);
					if(cust_depot_Id !=0 )
					outstandObj.put("CustomerDepot_Id_c", cust_depot_Id);
					long billToId = getCustomerId(billToParty);
					if(billToId != 0)
					outstandObj.put("BillToParty_Id_c",billToId );
					
					long shipToId = getCustomerId(shipToParty);
					if(shipToId !=0 )
					outstandObj.put("ShipToParty_Id_c", shipToId);
					
					long matrialId = getMaterialId(material);
					if(matrialId != 0)
					outstandObj.put("Material_Id_c", matrialId);
					//TODO Yet to add
					//outstandObj.put("Destination_c", destn);
					outstandObj.put("SalesQuantity_c", salesQty);
					outstandObj.put("GrossValue_c", grossVal);
					outstandObj.put("SONumber_Text_c", SONum);
					outstandObj.put("HSNCode_c", HSNCode);
					
					String jsonformat=outstandObj.toString();
					System.out.println("json"+jsonformat);
					if(!(distChnl.equals("NC") || distChnl.equals("ST")))
					{
					insertToOSC(jsonformat,logMsg);
					}
				}
				System.out.println("Reading next line");
				//logfil.getStringLog("\n");
			}
			br.close();
			
	            }
	            else
	            {
	            	System.out.println("No such File");
	            }
	        }
	    }
	} catch (Exception ee) {
		ee.printStackTrace();
		logfil.getExceptionLog(ee);
	} finally {
		try {
			br.close();
		} catch (IOException ie) {
			ie.printStackTrace();
			logfil.getExceptionLog(ie);
		}
	}
}
	public void insertToOSC(String outstandJson,String logMsg)
	{
		long salesRegNo=0L;
		try {
		String query=basequery+"SalesRegister_c/";
		HttpUrlMethodCall con=new HttpUrlMethodCall(query, username, password, "POST");
		System.out.println(query);
		String res=con.PostRequest(outstandJson);
		int rescode=con.StatusCode;
		String ResponseMsg = null;
		if(rescode > 200 && rescode < 400) {
			ResponseMsg = "Successfully Created";
			JSONObject OrgIDObj;
			OrgIDObj = new JSONObject(res);
			
			salesRegNo=Long.parseLong(OrgIDObj.get("Id").toString());
		}
		else {
			ResponseMsg = res;
			
		}
		System.out.println("Response::"+ResponseMsg);
		logfil.getStringLog(logMsg+","+ResponseMsg+","+salesRegNo);
	
		
		}catch(Exception e)
		{
			e.printStackTrace();
		}
	}
	public long getCustomerId(String CustomerSAPId)
	{
		String query=basequery+"accounts?q=OrganizationDEO_DealerCode_c=%20%27"+CustomerSAPId+"%27";
		HttpUrlMethodCall con=new HttpUrlMethodCall(query, username, password, "GET");
		String Response = con.GetRequest();
		String customerName=null;
		int responsecode=con.StatusCode;
		long customerId=0L;
//		 System.out.println("response"+Response+resCode);
		if (responsecode == 200) {
			if (Response != null && !"".equals(Response)
					&& !"null".equals(Response)) {
				try {
					JSONObject jsonInput1 = new JSONObject(Response);
					JSONArray ItemArray1 = (JSONArray) jsonInput1
							.get("items");
						if (ItemArray1 != null && ItemArray1.length() > 0) {
							JSONObject objj1 = (JSONObject) ItemArray1
									.get(0);
							if (objj1.isNull("OrganizationName") != true) {
								customerName = (String) objj1.getString("OrganizationName");
							}
							if (objj1.isNull("PartyId") != true) {
								customerId = (Long) objj1.getLong("PartyId");
							}
							
							
					//	System.out.println("customerName"+customerName);
					}
									
				}catch(Exception e) {e.printStackTrace();}
			}
		}
		return 	customerId;
	}

	public long getMaterialId(String material)
	{
		String query=basequery+"products?q=ItemNumber=%20%27"+material+"%27";
		HttpUrlMethodCall con=new HttpUrlMethodCall(query, username, password, "GET");
		String Response = con.GetRequest();
		int responsecode=con.StatusCode;
		long productId=0L;
//		 System.out.println("response"+Response+resCode);
		if (responsecode == 200) {
			if (Response != null && !"".equals(Response)
					&& !"null".equals(Response)) {
				try {
					JSONObject jsonInput1 = new JSONObject(Response);
					JSONArray ItemArray1 = (JSONArray) jsonInput1
							.get("items");
						if (ItemArray1 != null && ItemArray1.length() > 0) {
							JSONObject objj1 = (JSONObject) ItemArray1
									.get(0);
							if (objj1.isNull("InventoryItemId") != true) {
								productId = (Long) objj1.getLong("InventoryItemId");
							}
					}
									
				}catch(Exception e) {e.printStackTrace();}
			}
		}
		return 	productId;
	}

	public long getSourceLocation(String src)
	{
		String query=basequery+"PlantAndWarehouse_c?q=WarehouseCode_c=%27"+src+"%27";
		HttpUrlMethodCall con=new HttpUrlMethodCall(query, username, password, "GET");
		String Response = con.GetRequest();
		int responsecode=con.StatusCode;
		long plantWareHouseId=0L;
//		 System.out.println("response"+Response+resCode);
		if (responsecode == 200) {
			if (Response != null && !"".equals(Response)
					&& !"null".equals(Response)) {
				try {
					JSONObject jsonInput1 = new JSONObject(Response);
					JSONArray ItemArray1 = (JSONArray) jsonInput1
							.get("items");
						if (ItemArray1 != null && ItemArray1.length() > 0) {
							JSONObject objj1 = (JSONObject) ItemArray1
									.get(0);
							if (objj1.isNull("Id") != true) {
								plantWareHouseId = (Long) objj1.getLong("Id");
							}
		
	}
						return plantWareHouseId;
	}catch(Exception e)
				{
		e.printStackTrace();
				}
			}
		}
		return plantWareHouseId;
	}
	public void moveFiles(String currentdir,String currentfile)
	{
		try {
		Path temp = Files.move 
		        (Paths.get(currentdir+"\\"+currentfile), 
		        	Paths.get("D:\\FTP\\staging\\CRM\\Salesregister\\Archive\\"+currentfile)); 
		       	//	 Paths.get("E:\\Emami\\archive\\"+currentfile));
		        

		        if(temp != null) 
		        { 
		            System.out.println("File renamed and moved successfully"); 
		            logfil.getStringLog("File moved successfully");
		        } 
		        else
		        { 
		            System.out.println("Failed to move the file");
		            logfil.getStringLog("Failed to move the file");
		        }
		}
		catch(Exception e)
		{
			e.printStackTrace();
		}
	}
	public static void moveFilesInitial(String currentdir,String currentfile)
	{
		try {
		Path temp = Files.move 
		        (Paths.get(currentdir+"\\"+currentfile), 
		        	Paths.get("D:\\FTP\\staging\\CRM\\Salesregister\\Archive\\"+currentfile)); 
		       	//	 Paths.get("E:\\Emami\\archive\\"+currentfile));
		        

		        if(temp != null) 
		        { 
		            System.out.println("File renamed and moved successfully"); 
		            logfil.getStringLog("File moved successfully");
		            new SalesRegisterUpdate().getExcelDetails();
		        } 
		        else
		        { 
		            System.out.println("Failed to move the file");
		            logfil.getStringLog("Failed to move the file");
		        }
		}
		catch(Exception e)
		{
			e.printStackTrace();
		}
	}
	
	  public static String getYesterdayDate() {
	  
	  SimpleDateFormat dateFormat = new SimpleDateFormat("yyyyMMdd"); Calendar cal
	  = Calendar.getInstance(); cal.add(Calendar.DATE, -1); return
	  dateFormat.format(cal.getTime()); }
	 
	/*
	 * public static String getYesterdayDate() {
	 * 
	 * SimpleDateFormat dateFormat = new SimpleDateFormat("yyyyMM"); Calendar cal =
	 * Calendar.getInstance(); cal.add(Calendar.MONTH, -1); return
	 * dateFormat.format(cal.getTime()); }
	 */	
}