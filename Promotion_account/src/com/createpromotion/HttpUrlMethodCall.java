package com.createpromotion;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;

//import com.sun.corba.se.spi.activation.Repository;

import sun.misc.BASE64Encoder;

public class HttpUrlMethodCall {

	static String urlString = "";
	static String UserName = "";
	static String Password = "";
	String response = null;
	public int StatusCode = 0;
	String message = null;

	public HttpUrlMethodCall(String url, String username, String password, String MethodType) {
		urlString = url;
		UserName = username;
		Password = password;
	}

	/**
	 **** @Comments: For Saving Values to objects in Service or sales cloud
	 **/
	public String PostRequest(String Input) {
		response = null;
		StatusCode = 0;
		try {
			URL url = new URL(urlString);
			HttpURLConnection conn = (HttpURLConnection) url.openConnection();
			conn.setDoOutput(true);
			conn.setRequestMethod("POST");
			conn.setRequestProperty("Content-Type", "application/json");
			byte[] encodedPassword = (UserName + ":" + Password).getBytes();
			BASE64Encoder encoder = new BASE64Encoder();
			conn.setRequestProperty("Authorization", "Basic " + encoder.encode(encodedPassword));
			OutputStream os = conn.getOutputStream();
			os.write(Input.getBytes());
			os.flush();

			StatusCode = conn.getResponseCode();
			if (conn.getResponseCode() != HttpURLConnection.HTTP_CREATED) {
				message = "Failed : HTTP error code : " + conn.getResponseCode();
			}
			BufferedReader br = null;
			if (StatusCode > 200 && StatusCode < 400) {
				br = new BufferedReader(new InputStreamReader((conn.getInputStream())));
			} else {
				br = new BufferedReader(new InputStreamReader((conn.getErrorStream())));
			}
			StringBuilder builder = new StringBuilder();

			char[] buffer = new char[8192];
			int read;
			while ((read = br.read(buffer, 0, buffer.length)) > 0) {
				builder.append(buffer, 0, read);
			}
			response = builder.toString();
			conn.disconnect();
			message = response;
		} catch (MalformedURLException e) {
			message = "URL Exception in Post Method";
		} catch (IOException e) {
			message = "IO Exception in Post Method";
		}
		return message;
	}

	/**
	 **** @Comments: For getting Values from objects in Service or sales cloud
	 **/
	public String GetRequest() {
		response = null;
		StatusCode = 0;
		try {
			URL url = new URL(urlString);
			HttpURLConnection conn = (HttpURLConnection) url.openConnection();
			conn.setDoOutput(true);
			conn.setRequestMethod("GET");
			conn.setRequestProperty("Content-Type", "application/json");
			conn.setRequestProperty("REST-Framework-Version", "2");
			byte[] encodedPassword = (UserName + ":" + Password).getBytes();
			BASE64Encoder encoder = new BASE64Encoder();
			conn.setRequestProperty("Authorization", "Basic " + encoder.encode(encodedPassword));
			StatusCode = conn.getResponseCode();
			if (conn.getResponseCode() != HttpURLConnection.HTTP_OK) {
//                throw new RuntimeException("Failed : HTTP error code : "
//                        + conn.getResponseCode());
				message = "Failed : HTTP error code : " + conn.getResponseCode();
			}
			BufferedReader br = new BufferedReader(new InputStreamReader((conn.getInputStream())));
			StringBuilder builder = new StringBuilder();
			char[] buffer = new char[8192];
			int read;
			while ((read = br.read(buffer, 0, buffer.length)) > 0) {
				builder.append(buffer, 0, read);
			}
			response = builder.toString();

			conn.disconnect();
			message = response;
		} catch (MalformedURLException e) {
			message = "URL Exception in Get Method";
		} catch (IOException e) {
			message = "IO Exception in Get Method";
		}
		return message;
	}

	/**
	 **** @Comments: For updating Values to objects in Service or sales cloud
	 **/
	public String PutRequest(String Input) {
		response = null;
		StatusCode = 0;
		try {
			URL url = new URL(urlString);
			HttpURLConnection conn = (HttpURLConnection) url.openConnection();
			conn.setDoOutput(true);
			conn.setRequestMethod("POST");
			conn.setRequestProperty("X-HTTP-Method-Override", "PATCH");
			conn.setRequestProperty("Content-Type", "application/json");
			byte[] encodedPassword = (UserName + ":" + Password).getBytes();
			BASE64Encoder encoder = new BASE64Encoder();
			conn.setRequestProperty("Authorization", "Basic " + encoder.encode(encodedPassword));
			OutputStream os = conn.getOutputStream();
			os.write(Input.getBytes());
			os.flush();
			StatusCode = conn.getResponseCode();
			if (conn.getResponseCode() != HttpURLConnection.HTTP_CREATED) {
				// throw new RuntimeException("Failed : HTTP error code : "+
				// conn.getResponseCode());
				message = "Failed : HTTP error code : " + conn.getResponseCode();
			}
			BufferedReader br = null;
			if (StatusCode >= 200 && StatusCode < 400) {
				br = new BufferedReader(new InputStreamReader((conn.getInputStream())));
			}
			else {
				br = new BufferedReader(new InputStreamReader((conn.getErrorStream())));
			}
			StringBuilder builder = new StringBuilder();
			char[] buffer = new char[8192];
			int read;
			while ((read = br.read(buffer, 0, buffer.length)) > 0) {
				builder.append(buffer, 0, read);
			}
			response = builder.toString();
			conn.disconnect();
			message = response;
		} catch (MalformedURLException e) {
			message = "URL Exception in Patch Method";
		} catch (IOException e) {
			message = "IO Exception in Patch Method";
		}
		return message;
	}
}
