package com.createpromotion;
import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.PrintWriter;
import java.io.File;
import java.nio.file.Paths;
import java.util.Date;
import java.util.TimeZone;
import java.io.IOException;
import java.text.SimpleDateFormat;

public class LogFile {
	String FileName;
	String Foldername;
	Date currentdate = new Date();
	String file;
	SimpleDateFormat formatter = new SimpleDateFormat("ddMMyyyyHHmm");
	public void passfileName(String fileName)
	{
		this.file = fileName;
	}
	
	
public void Logfile_TimeZone(){
	formatter.setTimeZone(TimeZone.getTimeZone("IST"));
	String strDate = formatter.format(currentdate);
	Foldername = "D:\\FTP\\staging\\CRM\\Promotion\\LogFile";
	// Foldername ="E:\\Emami\\";
	FileName = Foldername + File.separator +"LogPromotion"+strDate+".txt";
}

	/**
	 **** @Comments: To get Exceptions
	 *
	 */

	public void getExceptionLog(Exception strexception) {
		Logfile_TimeZone();
		try {
			File file = new File(Foldername);
			if (!file.exists()) {
				if (file.mkdir()) {

					File fr = new File(FileName);
					FileWriter fw = new FileWriter(fr, true);
					BufferedWriter bw = new BufferedWriter(fw);
					PrintWriter out = new PrintWriter(bw);
					strexception.printStackTrace();
//					out.println("\n\n");
					out.close();
					bw.close();
					fw.close();
				} else {
				}
			} else {

				File fr1 = new File(FileName);
				FileWriter fw1 = new FileWriter(fr1, true);
				BufferedWriter bw1 = new BufferedWriter(fw1);
				PrintWriter out = new PrintWriter(bw1);

				strexception.printStackTrace();
//				out.println("\n\n");

				out.close();
				bw1.close();
				fw1.close();
			}
		} catch (Exception e) {

		}

	}

	/**
	 **** @Comments: To get response values
	 *
	 */
	public void getStringLog(String str) {
		Logfile_TimeZone();
		try {

			File file = new File(Foldername);
			if (!file.exists()) {
				if (file.mkdir()) {

					File fr1 = new File(FileName);
					FileWriter fw1 = new FileWriter(fr1, true);
					BufferedWriter bw1 = new BufferedWriter(fw1);
					PrintWriter out = new PrintWriter(bw1);
					out.println(str);
//					out.println("\n");
					out.close();
					bw1.close();
					fw1.close();
				} else {
				}
			} else {

				File fr1 = new File(FileName);
				FileWriter fw1 = new FileWriter(fr1, true);
				BufferedWriter bw1 = new BufferedWriter(fw1);
				PrintWriter out = new PrintWriter(bw1);
				out.println(str);
//				out.println("\n");
				out.close();
				bw1.close();
				fw1.close();
			}

		} catch (Exception e) {

		}

	}

}
