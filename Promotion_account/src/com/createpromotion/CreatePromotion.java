package com.createpromotion;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStream;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Date;

import org.json.JSONArray;
import org.json.JSONObject;
import org.apache.commons.codec.binary.Base64;
import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClientBuilder;
import com.createpromotion.HttpUrlMethodCall;
import com.createpromotion.LogFile;

public class CreatePromotion
{

int resCode = 0;
	String jsonformat = "";
	/*
	 * String basequery=
	 * "https://emwy-test.fa.em3.oraclecloud.com:443/crmRestApi/resources/11.13.18.05/";
	 * String username ="Admin Admin"; String password ="EMAMI_123";
	 * 
	 */
	String basequery="https://emwy.fa.em3.oraclecloud.com:443/crmRestApi/resources/11.13.18.05/";
	String username ="Admin";
	//String password ="EMAMI_123";
	String password ="Em@mi_123";
	LogFile logfil = new LogFile();
	private String _authToken;
	String _baseUrl;
	String _cloud;
	String strdelimetercomma = ",";
	//String downloadPath="E:\\Emami\\ExcelFile\\";
	String downloadPath="D:\\FTP\\staging\\CRM\\Promotion\\ExcelFile\\";
	public static void main(String args[])
	{
		
		CreatePromotion createprom=new CreatePromotion();
		createprom.updatePromotionStartEnd();
		createprom.createPromotionInAccounts();
		
	
	}
	public void updatePromotionStartEnd()
	{
		logfil.getStringLog("PromotionUpdate starts...");
		String todayDate= getTodaysDate();
		
		String query=basequery+"AccountPromotions_c?q=SchemeStartPeriod_c=%20%27"+todayDate+"%27";
		HttpUrlMethodCall con=new HttpUrlMethodCall(query, username, password, "GET");
		System.out.println("Promotion Start Date fetch-->"+query);
		String Response = con.GetRequest();
		int responsecode=con.StatusCode;
		
		long promotionId=0L;
		if (responsecode == 200) {
			if (Response != null && !"".equals(Response)
					&& !"null".equals(Response)) {
				try {
					JSONObject jsonInput1 = new JSONObject(Response);
					JSONArray ItemArray1 = (JSONArray) jsonInput1
							.get("items");
					System.out.println("jsonInput1"+jsonInput1);
					int length = ItemArray1.length();
					int i;
					for (i = 0; i < length; i++) {
						if (ItemArray1 != null && ItemArray1.length() > 0) {
							JSONObject objj1 = (JSONObject) ItemArray1
									.get(i);
							
							if(objj1.isNull("Id") != true)
							{
								promotionId=(Long)objj1.getLong("Id");
							
							}
							if(objj1.isNull("Status_c") != true)
							{
								String status=(String)objj1.get("Status_c");
								System.out.println("Status Start>."+status);
								if(!status.equalsIgnoreCase("ACTIVE"))
								{
							updatePromotion(promotionId,"START");
								}
							}
						}
					}
				}catch(Exception e) {
					e.printStackTrace();
					logfil.getExceptionLog(e);
				}
			}
		}
		String queryEnd=basequery+"AccountPromotions_c?q=SchemeEndPeriod_c%20%3C%20%27"+todayDate+"%27";
		HttpUrlMethodCall conEnd=new HttpUrlMethodCall(queryEnd, username, password, "GET");
		System.out.println("Promotion End Date Query-->"+queryEnd);
		String ResponseEnd = conEnd.GetRequest();
		int responsecodeEnd=conEnd.StatusCode;
		long promotionIdEnd=0L;
		
		if (responsecode == 200) {
			if (ResponseEnd != null && !"".equals(ResponseEnd)
					&& !"null".equals(ResponseEnd)) {
				try {
					JSONObject jsonInput1 = new JSONObject(ResponseEnd);
					JSONArray ItemArray1 = (JSONArray) jsonInput1
							.get("items");
					int length = ItemArray1.length();
					int i;
					System.out.println(jsonInput1);
					for (i = 0; i < length; i++) {
						if (ItemArray1 != null && ItemArray1.length() > 0) {
							JSONObject objj1 = (JSONObject) ItemArray1
									.get(i);
							
							if(objj1.isNull("Id") != true)
							{
								promotionId=(Long)objj1.getLong("Id");
							
							}
							if(objj1.isNull("Status_c") != true)
							{
								String status=(String)objj1.get("Status_c");
								System.out.println("Status >> "+status);
								if(!status.equalsIgnoreCase("INACTIVE"))
								{
									updatePromotion(promotionId,"END");
								}
							}
						}
					}
				}catch(Exception e) {
					e.printStackTrace();
					logfil.getExceptionLog(e);
				}
			}
		}
	
			
	}
	public void updatePromotion(long Id,String flagUpdt)
	{
		try {
		JSONObject promotionObj = new JSONObject(); 
		if(flagUpdt.equalsIgnoreCase("START"))
		promotionObj.put("Status_c","ACTIVE");
		if(flagUpdt.equalsIgnoreCase("END"))
		promotionObj.put("Status_c","INACTIVE");	
		String jsonformat =promotionObj.toString();
		String query=basequery+"AccountPromotions_c/"+Id;
		HttpUrlMethodCall con=new HttpUrlMethodCall(query, username, password, "PUT");
		String res=con.PutRequest(jsonformat);
		int rescode=con.StatusCode;
		String ResponseMsg = null;
		
		if(rescode >= 200 && rescode < 400) {
			ResponseMsg = "Successfully Updated";
		}
		else {
			ResponseMsg = res;
			
		}
		logfil.getStringLog(Id+","+ResponseMsg);
		System.out.println("updated");
		}
		catch(Exception e) {
			logfil.getExceptionLog(e);
			e.printStackTrace();
		}
		
	}
	public void createPromotionInAccounts()
	{
		logfil.getStringLog("Promotion Creation starts...");
		String todayDate= getTodaysDate();
		//String query=basequery+"Promotions_c?q=SchemeStartPeriod_c=%20%27"+todayDate+"%27%20and%20Status_c='INACTIVE'";
		String query=basequery+"Promotions_c?q=Account_Promotion_Flag_c='No'";
		HttpUrlMethodCall con=new HttpUrlMethodCall(query, username, password, "GET");
		
		String Response = con.GetRequest();
		int responsecode=con.StatusCode;
		long promotionId=0L;
		long customerId=0L;
		long OraZcxOwner_Id_c=0L;
		String SchemeName_c=null,SchemeEndPeriod_c=null,SchemeType_c=null,SchemeStartPeriod_c=null;
		// System.out.println("response"+Response+resCode);
		if (responsecode == 200) {
			if (Response != null && !"".equals(Response)
					&& !"null".equals(Response)) {
				try {
					JSONObject jsonInput1 = new JSONObject(Response);
					JSONArray ItemArray1 = (JSONArray) jsonInput1
							.get("items");
					int length = ItemArray1.length();
					int i;
					for (i = 0; i < length; i++) {
						Arrays.stream(new File(downloadPath).listFiles()).forEach(File::delete);
						if (ItemArray1 != null && ItemArray1.length() > 0) {
							JSONObject objj1 = (JSONObject) ItemArray1
									.get(i);
							
							if(objj1.isNull("SchemeName_c") != true)
							{
								SchemeName_c=(String)objj1.getString("SchemeName_c");
							
							}
							if(objj1.isNull("SchemeType_c") != true)
							{
								SchemeType_c=(String)objj1.getString("SchemeType_c");
							
							}
							if(objj1.isNull("SchemeStartPeriod_c") != true)
							{
								SchemeStartPeriod_c=(String)objj1.getString("SchemeStartPeriod_c");
							
							}
							if(objj1.isNull("SchemeEndPeriod_c") != true)
							{
								SchemeEndPeriod_c=(String)objj1.getString("SchemeEndPeriod_c");
							
							}
							if(objj1.isNull("OraZcxOwner_Id_c") != true)
							{
								OraZcxOwner_Id_c=(Long)objj1.getLong("OraZcxOwner_Id_c");
							
							}
							JSONObject promotionobj = new JSONObject();
							promotionobj.put("SchemeName_c", SchemeName_c);
							promotionobj.put("SchemeType_c", SchemeType_c);
							promotionobj.put("SchemeStartPeriod_c", SchemeStartPeriod_c);
							promotionobj.put("SchemeEndPeriod_c", SchemeEndPeriod_c);
							promotionobj.put("OraZcxOwner_Id_c", OraZcxOwner_Id_c);
							SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
					        Date date1 = sdf.parse(SchemeStartPeriod_c);
					        String todayLocalDate = LocalDate.now().toString();
					        Date date2 = sdf.parse(todayLocalDate);
					      //  System.out.println("Date1 schemadate>>"+date1+"::date2 currentdate"+date2+"::compareTo::"+date1.compareTo(date2));
					        if (date2.compareTo(date1) >= 0) {
					           // System.out.println("Date1 is after Date2");
					            promotionobj.put("Status_c", "ACTIVE");
					        } else
					        {
					        	promotionobj.put("Status_c", "NOT YET STARTED");
					        }
					        Date Enddate1 = sdf.parse(SchemeEndPeriod_c);
					      //  System.out.println("Enddate1 >>"+Enddate1+"::date2 currentdate"+date2+"::compareTo::"+date1.compareTo(date2));
					        if(Enddate1.compareTo(date2) < 0)
					        {
					        	//System.out.println("Date1 is before Date2");
					        	promotionobj.put("Status_c", "INACTIVE");
					        	
					        }
							
							String jsonpromo = promotionobj.toString();
							
							if (objj1.isNull("Id") != true) {
								 promotionId = (Long) objj1.getLong("Id");
								getFileContent(promotionId,jsonpromo);
								
							}
							
							
					//	System.out.println("customerName"+customerName);
					}
					}
									
				}catch(Exception e) {e.printStackTrace();
				logfil.getExceptionLog(e);
				}
			}
		}
		
	}
	public void createPromotionChild(String downloadFileName,long promotionId,String jsonpromo,String flgPromotion,int attachmentCount)
	{
		System.out.println("Creating promotion");
		BufferedReader br = null;
		try {
		//br = new BufferedReader(new FileReader("D:\\FTP\\staging\\CRM\\Promotion\\ExcelFile\\"+downloadFileName));
			System.out.println("Creating promotion from file>>"+downloadFileName);
			br = new BufferedReader(new FileReader(downloadFileName));
		String line = "";
		br.readLine();
	    String dealerCd=null;
	    int partyNo=0;
		if(downloadFileName.contains(".csv")){
			while ((line = br.readLine()) != null) {
				String[] dealerDetails = line.split(strdelimetercomma);
			
				if (dealerDetails.length > 0) {
					if (dealerDetails[0] != null) {
						dealerCd = dealerDetails[0];
						System.out.println("Dealer Code >>"+dealerCd);
					    partyNo = getAccountNo(dealerCd);
					    System.out.println("partyNo>>"+partyNo);
					    if(partyNo != 0)
					    {
								
					    	System.out.println("Creating child promotion.....");
							String query=basequery+"accounts/"+partyNo+"/child/OrganizationDEO_AccountPromotions_Account_AccountPromotions_Tgt/";
							try {
							HttpUrlMethodCall con=new HttpUrlMethodCall(query, username, password, "POST");
							String res=con.PostRequest(jsonpromo);
							int rescode=con.StatusCode;
							String ResponseMsg = null;
							rescode=200;
							if(rescode >= 200 && rescode < 400) {
								ResponseMsg = "Successfully Updated";
								JSONObject OrgIDObj;
								OrgIDObj = new JSONObject(res);
								long promo_no=0L;
								//promo_no=Long.parseLong("300000004627595");
								promo_no=Long.parseLong(OrgIDObj.get("Id").toString());
								System.out.println("Creating attachment for the promotion>>>>"+promo_no);
								
									
									File folder = new File(downloadPath);
									String[] files = folder.list();
									for (String file : files)
									{
										
													if(!file.contains(".csv"))
													{
														 File f =  new File(downloadPath+file);
														String encodedImg = encodeFileToBase64Binary(f);
															
														JSONObject attachemntJson=new JSONObject();
														attachemntJson.put("DatatypeCode","FILE");
														attachemntJson.put("FileName",file);
														attachemntJson.put("UploadedFileContentType","application/jpeg");
														attachemntJson.put("Title",file);
														attachemntJson.put("CategoryName","MISC");
														attachemntJson.put("FileContents",encodedImg);
														
														String queryAttchmt=basequery+"AccountPromotions_c/"+promo_no+"/child/Attachment/";
														HttpUrlMethodCall conattachment1=new HttpUrlMethodCall(queryAttchmt, username, password, "POST");
														
														String resAttachmnt=conattachment1.PostRequest(attachemntJson.toString());
												//		System.out.println(resAttachmnt);
														System.out.println("queryAttchmt>>"+queryAttchmt);
												//		System.out.println(conattachment1);
														int Attchmntresponsecode=conattachment1.StatusCode;
														String responsefromattmt = null;
														System.out.println("Attchmntresponsecode"+Attchmntresponsecode);
														if(Attchmntresponsecode >= 200 && Attchmntresponsecode < 400) {
															responsefromattmt = "Successfully attachment created";
												//			System.out.println(responsefromattmt);
															f.delete();
														}
													
													}

												}
							}
												
							else {
								ResponseMsg = res;
								
							}
							JSONObject promotnupdt = new JSONObject();
							promotnupdt.put("Account_Promotion_Flag_c","Yes");
							String queryRetrnUpdt=basequery+"Promotions_c/"+promotionId;
							HttpUrlMethodCall conattachment=new HttpUrlMethodCall(queryRetrnUpdt, username, password, "PUT");
							String resAttachmnt=conattachment.PutRequest(promotnupdt.toString());
							int responsecode=conattachment.StatusCode;
							String responsefromattmt = null;
							
							if(responsecode >= 200 && responsecode < 400) {
								responsefromattmt = "Successfully promotion flag updated";
							//	System.out.println(responsefromattmt);
							}
							System.out.println("updated to account"+ResponseMsg);
							logfil.getStringLog(partyNo+","+ResponseMsg);
							
							
					    }catch(Exception e)
							{
					    	e.printStackTrace();
					    	logfil.getExceptionLog(e);
							}
					    
					}
					System.out.println("dealerCd:"+dealerCd+"partyNo::"+partyNo);
					logfil.getStringLog("dealerCd:"+dealerCd+"partyNo::"+partyNo);
				}
		}
		}
		}else
		{
			//FileUtils.cleanDirectory(downloadPath);
			//Arrays.stream(new File(downloadPath).listFiles()).forEach(File::delete);
		}
		}
		catch (Exception ee) {
			ee.printStackTrace();
			logfil.getExceptionLog(ee);
		} finally {
			try {
				br.close();
			} catch (IOException ie) {
				ie.printStackTrace();
				logfil.getExceptionLog(ie);
			}
		}
	}
	
	public int getAccountNo(String dealerCode)
	{
		String query=basequery+"accounts?q=OrganizationDEO_DealerCode_c=%20%27"+dealerCode+"%27";
		HttpUrlMethodCall con=new HttpUrlMethodCall(query, username, password, "GET");
		String Response = con.GetRequest();
		String customerName=null;
		int responsecode=con.StatusCode;
		int partyNo=0;
//		 System.out.println("response"+Response+resCode);
		if (responsecode == 200) {
			if (Response != null && !"".equals(Response)
					&& !"null".equals(Response)) {
				try {
					JSONObject jsonInput1 = new JSONObject(Response);
					JSONArray ItemArray1 = (JSONArray) jsonInput1
							.get("items");
						if (ItemArray1 != null && ItemArray1.length() > 0) {
							JSONObject objj1 = (JSONObject) ItemArray1
									.get(0);
							if (objj1.isNull("PartyNumber") != true) {
								partyNo = (int) objj1.getInt("PartyNumber");
							}
							
						System.out.println("partyNo"+partyNo);
					}
									
				}catch(Exception e) {e.printStackTrace();
				logfil.getExceptionLog(e);}
			}
		}
		return 	partyNo;
	}
	public void getFileContent(long Id,String jsonpromo)  {
		String filename = null,retFileName = null;
		String searchattachment = "https://emwy-test.fa.em3.oraclecloud.com:443/crmRestApi/resources/11.13.18.05/Promotions_c/"
				+ Id + "/child/Attachment";
		try {
		HttpUrlMethodCall con=new HttpUrlMethodCall(searchattachment, username, password, "GET");
		String acc_Response=con.GetRequest();
		System.out.println("Downloading File");
		//System.out.println(acc_Response);
		int responsecode=con.StatusCode;
		System.out.println(resCode);
		if (responsecode >= 200 && responsecode < 400) {
			
			if (acc_Response != null && !"".equals(acc_Response)
					&& !"null".equals(acc_Response)) {
				JSONObject accJson = new JSONObject(acc_Response);
				JSONArray accArray = (JSONArray) accJson.get("items");
				long attachmid = 0;
				
				int len = accArray.length();
				System.out.println("len"+len);
				 JSONObject attachemntJson = new JSONObject();
				//if (len == 1) {
					 for (int i = 0; i < len; i++) {

					if (accArray != null && accArray.length() > 0) {
						JSONObject obj1 = (JSONObject) accArray.get(i);
						if (obj1.isNull("AttachedDocumentId") != true) {
							attachmid = (long) obj1
									.getLong("AttachedDocumentId");

						}
						if (obj1.isNull("FileName") != true) {
							filename = obj1.getString("FileName");

						}
                        System.out.println("FileName"+filename);
						JSONArray linkarr = (JSONArray) obj1.get("links");
						int linklen = linkarr.length();
						String name = null, href = null;
						// for(int ll=3;ll<linklen;ll++)
						// {
						JSONObject objnew = (JSONObject) linkarr.get(3);
						if (objnew.isNull("name") != true) {
							name = objnew.getString("name");

						}

						if (objnew.isNull("href") != true) {
							href = objnew.getString("href");

						}

							/*
							 * if(!filename.contains(".csv")) {
							 * attachemntJson=getFileUploadToPromotion(Id,filename); }
							 */

						 
						FileDownload(Id, name, href, filename);
							
						
								  
								 
						
					}
				}
					 File[] filesInDirectory = new File(downloadPath).listFiles();
					 for(File f : filesInDirectory){
					     String filePath = f.getAbsolutePath();
					     String fileExtenstion = filePath.substring(filePath.lastIndexOf(".") + 1,filePath.length());
					     
					     if("csv".equals(fileExtenstion)){
							
							retFileName = f.getAbsolutePath();
							createPromotionChild(retFileName,Id,jsonpromo,"START",len);
						} 
					 }
					
			}
			
		}
		
		}catch (Exception e) {
			e.printStackTrace();
			logfil.getExceptionLog(e);
		}
		

	}
	private JSONObject getFileUploadToPromotion(long Id,String filename)
	{
		JSONObject attachemntJson = new JSONObject();
	String searchattachment = "https://emwy-test.fa.em3.oraclecloud.com:443/crmRestApi/resources/11.13.18.05/Promotions_c/"
			+ Id + "/child/Attachment?q=FileName=%27"+filename.replace(" ", "%20")+"%27";
	try {
	HttpUrlMethodCall con=new HttpUrlMethodCall(searchattachment, username, password, "GET");
	String acc_Response=con.GetRequest();
	System.out.println("Downloading File" + searchattachment);
	System.out.println("Downloaded File" + acc_Response);
	//System.out.println(acc_Response);
	int responsecode=con.StatusCode;
	System.out.println(resCode);
	String DatatypeCode=null,UploadedFileContentType=null,FileUrl=null,CategoryName=null,DmDocumentId=null,DmVersionNumber=null,Title=null;
	if (responsecode >= 200 && responsecode < 400) {
		
		if (acc_Response != null && !"".equals(acc_Response)
				&& !"null".equals(acc_Response)) {
			JSONObject accJson = new JSONObject(acc_Response);
			JSONArray accArray = (JSONArray) accJson.get("items");
			long attachmid = 0;
			
			int len = accArray.length();
			System.out.println("len"+len);
			
			if (accArray != null && accArray.length() > 0) {
				JSONObject obj1 = (JSONObject) accArray.get(0);
				if (obj1.isNull("AttachedDocumentId") != true) {
					attachmid = (long) obj1
							.getLong("AttachedDocumentId");
					

				}
				if (obj1.isNull("DatatypeCode") != true) {
					DatatypeCode = (String) obj1
							.getString("DatatypeCode");
					attachemntJson.put("DatatypeCode", DatatypeCode);

				}
				if (obj1.isNull("UploadedFileContentType") != true) {
					UploadedFileContentType = (String) obj1
							.getString("UploadedFileContentType");
					attachemntJson.put("UploadedFileContentType", UploadedFileContentType);

				}
				if (obj1.isNull("FileUrl") != true) {
					FileUrl = (String) obj1
							.getString("FileUrl");
					attachemntJson.put("FileUrl", FileUrl);

				}
				if (obj1.isNull("CategoryName") != true) {
					CategoryName = (String) obj1
							.getString("CategoryName");
					attachemntJson.put("CategoryName", CategoryName);
				}
						
						  if (obj1.isNull("DmDocumentId") != true) { DmDocumentId = (String) obj1
						  .getString("DmDocumentId"); attachemntJson.put("DmDocumentId", DmDocumentId);
						  
						  }
						
						  if (obj1.isNull("DmVersionNumber") != true) { DmVersionNumber = (String) obj1
						  .getString("DmVersionNumber"); attachemntJson.put("DmVersionNumber",
						  DmVersionNumber); }
						 
				if (obj1.isNull("Title") != true) {
					Title = (String) obj1
							.getString("Title");
					attachemntJson.put("Title", Title);

				}

			}
		}
	}
	}
	catch(Exception e) {e.printStackTrace();
	logfil.getExceptionLog(e);}
	return attachemntJson;
	}
	private void FileDownload(long Id, String name, String href, String FileName) {
		_baseUrl = "emwy-test.fa.em3.oraclecloud.com:443/crmRestApi/resources/11.13.18.05/Promotions_c/"
				+ Id + "/child/Attachment";
		String authString = username + ":" + password;
		_authToken = "Basic "
				+ javax.xml.bind.DatatypeConverter.printBase64Binary(authString
						.getBytes());
		try {

			CloseableHttpClient client = HttpClientBuilder.create().build();
			
			HttpGet request = new HttpGet(href);
			request.addHeader("Authorization", _authToken);
			HttpResponse response = client.execute(request);
			HttpEntity entity = response.getEntity();

			int responseCode = response.getStatusLine().getStatusCode();

			InputStream is = entity.getContent();
//			String filePath = "/home/ubuntu/TEST/Bulk_Test_Email/Excel/"+FileName;
			//String filePath = "D:\\FTP\\staging\\CRM\\Promotion\\ExcelFile\\"+FileName;
			System.out.println("Get file Name"+FileName);
			String filePath = downloadPath+FileName;
			File file = new File(filePath);
			if (file.exists())
				file.delete();
			if (!file.exists()) {
				FileOutputStream fos = new FileOutputStream(new File(filePath));

				int inByte;
				while ((inByte = is.read()) != -1) {
					fos.write(inByte);
				}

				is.close();
				fos.close();

				client.close();
			}

		} catch (ClientProtocolException e) {
			e.printStackTrace();
			logfil.getExceptionLog(e);
		} catch (UnsupportedOperationException e) {
			e.printStackTrace();
			logfil.getExceptionLog(e);
		} catch (IOException e) {
			e.printStackTrace();
			logfil.getExceptionLog(e);
		}
	}
	
	public static String getTodaysDate() {

	    SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
	    Calendar cal = Calendar.getInstance();
	  //  cal.add(Calendar.DATE, 1);
	    return dateFormat.format(cal.getTime());
	}
	 private static String encodeFileToBase64Binary(File file) throws Exception{
         FileInputStream fileInputStreamReader = new FileInputStream(file);
         byte[] bytes = new byte[(int)file.length()];
         fileInputStreamReader.read(bytes);
         return new String(Base64.encodeBase64(bytes), "UTF-8");
     }
}