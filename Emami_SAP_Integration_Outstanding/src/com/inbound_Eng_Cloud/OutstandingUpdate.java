//Version:1.00
/**
 ****@Program: Insert/Update into the Outstanding object in engagement cloud based on the values from the sap id in excel 
 * */
package com.inbound_Eng_Cloud;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.util.Calendar;
import java.util.Date;

import org.json.JSONArray;
import org.json.JSONObject;

public class OutstandingUpdate {
	public static LogFile logfil = new LogFile();
	int resCode = 0;
	String jsonformat = "";
	//String basequery="https://emwy-test.fa.em3.oraclecloud.com:443/crmRestApi/resources/11.13.18.05/";
	String basequery="https://emwy.fa.em3.oraclecloud.com:443/crmRestApi/resources/11.13.18.05/";
	String username ="API USER";
	//String password ="EMAMI_123";
	String password ="API@1234";
	
	private static final String strdelimetercomma = ",";
	public static void main(String[] args) {
		OutstandingUpdate outstnd=new OutstandingUpdate();
		outstnd.getExcelDetails();
	}
	
	
	public void getExcelDetails()
	{
	BufferedReader br = null;
	try {
		//File folder = new File("E:\\Emami\\TEST"); /*path to your folder*/
		 File folder = new File("D:\\FTP\\staging\\CRM\\CustomerOutstanding\\Outbound\\");
		String[] filesPresent = folder.list();
	  //  System.out.println(filesPresent.length);
	    
	    if(filesPresent.length==0){
	        System.out.println("Nothing to delete");
	        
	    }else{
	    	
	    	
	        for(String fileName : filesPresent){  
	        	System.out.println(fileName);
	        	System.out.println(getYesterdayDate());
	            if(fileName.toLowerCase().endsWith(".csv") && fileName.toLowerCase().contains("outstanding") && fileName.toLowerCase().contains(getYesterdayDate().toString())){
	              System.out.println("entered");
	                File file = new File(fileName);
	                logfil.passfileName(file.getName());
	                System.out.println(file.getName());
	              
		String downloadfilename=file.getName();
		System.out.println(downloadfilename);
		br = new BufferedReader(new FileReader("D:\\FTP\\staging\\CRM\\CustomerOutstanding\\Outbound\\"+downloadfilename ));
	//	br = new BufferedReader(new FileReader("E:\\Emami\\TEST\\"+downloadfilename ));
		String line = "";
		//br.readLine();
		logfil.getStringLog( "CustomerSAPId"+","+"CustomerName"+","+"CustomerCredt"+","+"CustomerBal"+","+"Cust0_7D"+","+"Cust8_15D"+","+"Cust16_30D"+","+"Cust31_60D"+","+"Cust61_90D"+","+"Cust91_180D"+","+"Cust181_9999D"+","+"Response"+","+"OutstandingId");		
			while ((line = br.readLine()) != null) {
				String CustomerSAPId=null;
				String CustomerName=null,CustomerCredt=null,CustomerBal=null,Cust0_7D=null,Cust8_15D=null,Cust16_30D=null,Cust31_60D=null,Cust61_90D=null,Cust91_180D=null,Cust181_9999D=null;
				String[] outstandingDetails = line.split(strdelimetercomma);
				System.out.println("outstandingDetails.length::"+outstandingDetails.length);
				if (outstandingDetails.length > 0) {
					if (outstandingDetails[0] != null) {
						CustomerSAPId = outstandingDetails[0];
					} else {
						CustomerSAPId = null;
					}
					if (outstandingDetails[1] != null) {
						CustomerName = outstandingDetails[1];
					} else {
						CustomerName = null;
					}
					if (outstandingDetails[2] != null) {
						CustomerCredt = outstandingDetails[2];
					} else {
						CustomerCredt = null;
					}
					if (outstandingDetails[3] != null) {
						CustomerBal = outstandingDetails[3];
					} else {
						CustomerBal = null;
					}
					if (outstandingDetails[4] != null) {
						Cust0_7D = outstandingDetails[4].trim().split("\\.")[0];
					} else {
						Cust0_7D = null;
					}
					if (outstandingDetails[5] != null) {
						Cust8_15D = outstandingDetails[5].split("\\.")[0];
					} else {
						Cust8_15D = null;
					}
					if (outstandingDetails[6] != null) {
						Cust16_30D = outstandingDetails[6].split("\\.")[0];
					} else {
						Cust16_30D = null;
					}
					if (outstandingDetails[7] != null) {
						Cust31_60D = outstandingDetails[7].split("\\.")[0];
					} else {
						Cust31_60D = null;
					}
					if (outstandingDetails[8] != null) {
						Cust61_90D = outstandingDetails[8].split("\\.")[0];
					} else {
						Cust61_90D = null;
					}
					if (outstandingDetails[9] != null) {
						Cust91_180D = outstandingDetails[9].split("\\.")[0];
					} else {
						Cust91_180D = null;
					}
					if (outstandingDetails[10] != null) {
						Cust181_9999D = outstandingDetails[10].split("\\.")[0];
					} else {
						Cust181_9999D = null;
					}
					
					//System.out.println("CustomerSAPID::" + CustomerSAPId+"::CustomerName::"+CustomerName+"::Customer Credit::"+CustomerCredt+"::Customer Balance::"+CustomerBal+"::0-7 Days::"+Cust0_7D+"::8-15 Days::"+Cust8_15D+"::16-30 Days::"+Cust16_30D+"::31-60 Days::"+Cust31_60D+"::61-90 Days::"+Cust61_90D+"::91-180 Days::"+Cust91_180D+"::181-9999 Days"+Cust181_9999D);
					//logfil.getStringLog("CustomerSAPID::" + CustomerSAPId+"::CustomerName::"+CustomerName+"::Customer Credit::"+CustomerCredt+"::Customer Balance::"+CustomerBal+"::0-7 Days::"+Cust0_7D+"::8-15 Days::"+Cust8_15D+"::16-30 Days::"+Cust16_30D+"::31-60 Days::"+Cust31_60D+"::61-90 Days::"+Cust61_90D+"::91-180 Days::"+Cust91_180D+"::181-9999 Days"+Cust181_9999D);
					String logMsg= CustomerSAPId+","+CustomerName+","+CustomerCredt+","+CustomerBal+","+Cust0_7D+","+Cust8_15D+","+Cust16_30D+","+Cust31_60D+","+Cust61_90D+","+Cust91_180D+","+Cust181_9999D;
					long custId = 0L;
					custId = getCustomerId(CustomerSAPId);
					JSONObject outstandObj=new JSONObject();
					if(custId != 0)
					{
					outstandObj.put("CustomerNameNew_Id_c", custId);
					}
					outstandObj.put("CustomerSAPId_c", CustomerSAPId);
					outstandObj.put("CustomerCreditLimit_c", CustomerCredt);
					outstandObj.put("CustomerBalance_c", CustomerBal);
					outstandObj.put("C0to7Days_c", Cust0_7D);
					outstandObj.put("C08to15Days_c", Cust8_15D);
					outstandObj.put("C16to30Days_c", Cust16_30D);
					outstandObj.put("C31to60Days_c", Cust31_60D);
					outstandObj.put("C61to90Days_c", Cust61_90D);
					outstandObj.put("C91to180Days_c", Cust91_180D);
					outstandObj.put("C181to9999Days_c", Cust181_9999D);
					String jsonformat=outstandObj.toString();
					checkOutstandingRcrdExists(CustomerSAPId,jsonformat,logMsg);
					
				}
				System.out.println("Reading next line");
				//logfil.getStringLog("\n");
			}
			 br.close();
				moveFiles(folder.getAbsolutePath(),downloadfilename) ;
				
			}
	             
	        }
	    }
	} catch (Exception ee) {
		ee.printStackTrace();
		logfil.getExceptionLog(ee);
	} finally {
		try {
			if(br == null){
			     System.out.println("is closed");
			 }
			 else{
				 br.close();
			     System.out.println("is open");
			 }
			
			
		} catch (IOException ie) {
			ie.printStackTrace();
			logfil.getExceptionLog(ie);
		}
	}
}
	public void checkOutstandingRcrdExists(String CustomerSAPId,String jsonformat,String logMsg)
	{
		String query=basequery+"Outstanding_c?q=CustomerSAPId_c=%20%27"+CustomerSAPId+"%27";
		HttpUrlMethodCall con=new HttpUrlMethodCall(query, username, password, "GET");
		String Response = con.GetRequest();
		int responsecode=con.StatusCode;
		long outstandingId=0L;
		System.out.println(query);
		 System.out.println("response"+Response+resCode);
		if (responsecode == 200) {
			if (Response != null && !"".equals(Response)
					&& !"null".equals(Response)) {
				try {
					JSONObject jsonInput1 = new JSONObject(Response);
					JSONArray ItemArray1 = (JSONArray) jsonInput1
							.get("items");
						if (ItemArray1 != null && ItemArray1.length() > 0) {
							JSONObject objj1 = (JSONObject) ItemArray1
									.get(0);
							if (objj1.isNull("Id") != true) {
								outstandingId = (Long) objj1.getLong("Id");
							}
							
							
			//			System.out.println("outstandingId"+outstandingId);
					}
									
				}catch(Exception e) {e.printStackTrace();}
			}
			System.out.println("outstandingId"+outstandingId+"outstandJson"+jsonformat);
		if(outstandingId==0)
		{
			
			insertToOSC(jsonformat,logMsg);
		}
		else
		{
			updateToOSC(jsonformat,outstandingId,logMsg);
		}
		}	
	}
	public void updateToOSC(String outstandJson,long outStandingId,String logMsg)
	{
	try {
		String query=basequery+"Outstanding_c/"+outStandingId;
		HttpUrlMethodCall con=new HttpUrlMethodCall(query, username, password, "PUT");
		String res=con.PutRequest(outstandJson);
		int rescode=con.StatusCode;
		String ResponseMsg = null;
		
		if(rescode >= 200 && rescode < 400) {
			ResponseMsg = "Successfully Updated";
		}
		else {
			ResponseMsg = res;
			
		}
		logfil.getStringLog(logMsg+","+ResponseMsg+","+outStandingId);
	System.out.println("updated");
		
		}catch(Exception e)
		{
			e.printStackTrace();
		}
	}
	public void insertToOSC(String outstandJson,String logMsg)
	{
		long outstanNo=0L;
		try {
		String query=basequery+"Outstanding_c/";
		HttpUrlMethodCall con=new HttpUrlMethodCall(query, username, password, "POST");
		String res=con.PostRequest(outstandJson);
		int rescode=con.StatusCode;
		String ResponseMsg = null;
		System.out.println("rescode>>>"+rescode);
		if(rescode > 200 && rescode < 400) {
			ResponseMsg = "Successfully Created";
			JSONObject OrgIDObj;
			OrgIDObj = new JSONObject(res);
			
			outstanNo=Long.parseLong(OrgIDObj.get("Id").toString());
		}
		else {
			ResponseMsg = res;
			if(res.contains("Outstanding_calculation"))
				{
					ResponseMsg = "Account Master data is not available";
				}
			
		}
		logfil.getStringLog(logMsg+","+ResponseMsg+","+outstanNo);
		System.out.println("Created"+res);
	
		
		}catch(Exception e)
		{
			e.printStackTrace();
		}
	}
public long getCustomerId(String CustomerSAPId)
{
	String query=basequery+"accounts?q=OrganizationDEO_DealerCode_c=%20%27"+CustomerSAPId+"%27";
	HttpUrlMethodCall con=new HttpUrlMethodCall(query, username, password, "GET");
	String Response = con.GetRequest();
	String customerName=null;
	int responsecode=con.StatusCode;
	long customerId=0L;
	if (responsecode == 200) {
		if (Response != null && !"".equals(Response)
				&& !"null".equals(Response)) {
			try {
				JSONObject jsonInput1 = new JSONObject(Response);
				JSONArray ItemArray1 = (JSONArray) jsonInput1
						.get("items");
					if (ItemArray1 != null && ItemArray1.length() > 0) {
						JSONObject objj1 = (JSONObject) ItemArray1
								.get(0);
						if (objj1.isNull("OrganizationName") != true) {
							customerName = (String) objj1.getString("OrganizationName");
						}
						if (objj1.isNull("PartyId") != true) {
							customerId = (Long) objj1.getLong("PartyId");
						}
						
				}
								
			}catch(Exception e) {e.printStackTrace();}
		}
	}
	return 	customerId;
}
public void moveFiles(String currentdir,String currentfile)
{
	try {
	Path temp = Files.move 
	        (Paths.get(currentdir+"\\"+currentfile), 
	        		Paths.get("D:\\FTP\\staging\\CRM\\CustomerOutstanding\\Archive"+currentfile)); 
	       		 //Paths.get("E:\\Emami\\archive\\"+currentfile));
	        

	        if(temp != null) 
	        { 
	            System.out.println("File renamed and moved successfully"); 
	            logfil.getStringLog("File moved successfully");
	        } 
	        else
	        { 
	            System.out.println("Failed to move the file");
	            logfil.getStringLog("Failed to move the file");
	        }
	}
	catch(Exception e)
	{
		e.printStackTrace();
	}
}
public static String getYesterdayDate() {

    SimpleDateFormat dateFormat = new SimpleDateFormat("yyyyMMdd");
    Calendar cal = Calendar.getInstance();
    cal.add(Calendar.DATE, -1);
    return dateFormat.format(cal.getTime());
}

}